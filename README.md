### Table of Contents

- [About](#about)
- [Requirements](#requirements)
  * [PHP](#php)
  * [Bootstrap](#bootstrap)
  * [Web Server](#web-server)
- [Directory layout](#directory-layout)
- [Deployment instructions](#deployment-instructions)
  * [standalone](#standalone)
  * [webserver](#webserver)
    + [httpd/apache](#httpd-apache)
    + [nginx](#nginx)
- [Adding new crypto currencies](#adding-new-crypto-currencies)
- [Converting coins](#converting-coins)

### About

Convert popular crypto coins into available currencies.

![](demo/crypto_converter_demo.png)

### Requirements
#### PHP

* PHP 7.x

#### Bootstrap

* Uses an offline version of bootstrap - http://getbootstrap.com/docs/3.3/getting-started/#download

#### Web Server

* optional nginx/httpd web server

### Directory layout
```
crypto_converter # www_root
├── classes
│   ├── Coin.class.php
│   ├── Logger.class.php
│   └── Validate.class.php
├── conf
│   └── config.php
├── css
│   ├── bootstrap-theme.css
│   ├── bootstrap-theme.css.map
│   ├── bootstrap-theme.min.css
│   ├── bootstrap-theme.min.css.map
│   ├── bootstrap.css
│   ├── bootstrap.css.map
│   ├── bootstrap.min.css
│   └── bootstrap.min.css.map
├── favicon.ico
├── fonts
│   ├── glyphicons-halflings-regular.eot
│   ├── glyphicons-halflings-regular.svg
│   ├── glyphicons-halflings-regular.ttf
│   ├── glyphicons-halflings-regular.woff
│   └── glyphicons-halflings-regular.woff2
├── images
│   ├── BiteMyCoin-Cryptocurrency.png
│   ├── Cryptocurrency-MobileCover.jpg
│   ├── credits.txt
│   └── favicon-192.png
├── includes
│   └── convert.php
├── index.php
└── js
    ├── bootstrap.js
    ├── bootstrap.min.js
    └── npm.js

7 directories, 2 files
```
### Deployment instructions

* the crypto_converter (www root directory) needs to be placed in the web root of the webserver

#### standalone
* within the www_root dir (cypto_converter) run

```
php -S localhost:3000
```

* navigate to http://localhost:3000 with a web browser

#### webserver

##### httpd/apache

* place the cypto_converter in the defined DocumentRoot directive - https://httpd.apache.org/docs/2.4/mod/core.html#documentroot
* e.g: ```/var/www/sites``` in unix based systems, or ```c:\www\sites``` in windows based systems

##### nginx

* place the cypto_converter in the defined root directive - http://nginx.org/en/docs/http/ngx_http_core_module.html#root
* e.g: ```/var/www/sites``` in unix based systems, or ```c:\www\sites``` in windows based systems 


### Adding new crypto currencies

* New coins can be added by editing ```cypto_converter/conf/config.php```

* Adding a new currency into the ```$available_currencies``` array; for example:

```
// array of currency codes in lowercase
// requires matching key entry in $crypto_coins array 
// used to display currency drop down menu in form
   
$available_currencies = array(
	"zar",
	"eur",
);

```
* Adding a new crypto coin into the ```$available_coins``` array; for example:

```
// array of crypto coin descriptions in lowercase
// requires matching key entry in $crypto_coins array
// used to display crypto coins drop down menu in form

$available_coins = array(
	"bitcoin",
	"ethereum",
	"monero",
);

```
* Including an additional array in ```$crypto_coins``` using the following format;
* NB new currency must match keys: eur/zar (or another currency) and crypto coin must match the desc value, for example:

```
$crypto_coins = array(
...
array(
	"desc"         => "bitcoin",
	"code"         => "BTC",
	"eur"          => 12272.77,
	"zar"          => 184707.34,
	"last_updated" => "2017-12-24",
),
```        

### Converting coins

* Input a valid amount (negative numbers are not allowed) for example: ```0,01```

![](demo/convert_amount_demo.png)

* Using the drop down menu, choose an available Cryto currency, for example: ```MONERO```

![](demo/convert_crypto_coin_demo.png)

* Using the drop down menu, choose an available Currency, for example: ```ZAR```

![](demo/convert_currency_demo.png)

* Click on the Convert button, the following are displayed
* Results header: Timestamp with conversion results, for example: 

```
OK: 2018-01-14 06:58:09 0.01 XMR in ZAR is 50.07
```

* Last Updated header: Timestamp of last conversion sync, for example: 

```
[2018-01-06] 1 XMR = 5006.72 ZAR
```

![](demo/convert_result_demo.png)