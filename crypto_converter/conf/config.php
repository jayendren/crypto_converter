<?php
/*
Static configuration and data for converter

    $available_currencies
        array of currency codes in lowercase

    $available_coins
        array of crypto coin descriptions in lowercase

    $crypto_coins
        2d array containing:
            - index
            - description (desc)
            - coin code (code)
            - value in EURO (eur) (must match $available_currencies)
            - value in ZAR (zar) (must match $available_currencies)
            - last manual update via https://www.coingecko.com (last_updated)

    $footer
        index.php footer text                    

Author
    Jay Maduray <jayendren@gmail.com>        
*/

// array of currency codes in lowercase
// requires matching key entry in $crypto_coins array 
// used to display currency drop down menu in form   
$available_currencies = array(
    "zar",
    "eur",
);
// array of crypto coin descriptions in lowercase
// requires matching key entry in $crypto_coins array
// used to display crypto coins drop down menu in form
$available_coins = array(
    "bitcoin",
    "ethereum",
    "monero",
);
// 2d array of coins
// code reuse
// Ref: http://webcheatsheet.com/php/multidimensional_arrays.php
$crypto_coins = array(
    array(
        "desc"         => "bitcoin",
        "code"         => "BTC",
        "eur"          => 12272.77,
        "zar"          => 184707.34,
        "last_updated" => "2017-12-24",
    ),    
    array(
        "desc"         => "ethereum",
        "code"         => "ETH",
        "eur"          => 623.83,
        "zar"          => 9229.65,
        "last_updated" => "2017-12-29",
    ),
    array(
        "desc"         => "monero",
        "code"         => "XMR",
        "eur"          => 338.09,
        "zar"          => 5006.72,
        "last_updated" => "2018-01-06",
    ),    
);
// index footer text
$footer = "<center>v2.1 2017-2018 Jay Maduray.</center>";