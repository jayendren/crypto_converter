<?php

/*
convert.php

	This script is used by index.php to consume input data;
	validate and calculate conversion to selected currency.

	Function convert_coin accepts $crypto_coin 2d array (config.php) and form inputs,
    and stores results in $html_result array for browser consumption in index.php.

Author
	Jay Maduray <jayendren@gmail.com>
*/

include_once "conf/config.php"; 		// static configuration
include_once "classes/Coin.class.php"; // Coin class

// variables from form input
$form_amount   = $_POST['amount'];
$form_crypto   = $_POST['crypto'];
$form_currency = $_POST['currency'];

// functions
function convert_coin(
    $crypto_coins,
    $amount,
    $crypto,
    $currency
) {

    // logger object
    $convertLog = new Logger;
    // required input cannot be empty;
    // coin_data must be an array
    if (is_array($crypto_coins) && !empty($amount) && !empty($crypto) && !empty($currency)) {
        // code reuse
        // Ref: https://stackoverflow.com/questions/6661530/php-multidimensional-array-search-by-value
        $find_coin = array_search($crypto, array_column($crypto_coins, 'desc'));

        $myCoin = new Coin($crypto_coins[$find_coin]);

        // validates amount is a positive number
        if ($myCoin->validateCoin($amount)) {
            $coin_value         = $myCoin->getCoin($currency);
            $coin_converted     = $myCoin->calcCoin($amount, $coin_value);
            $coin_code          = $myCoin->getCoin("code");
            $fmt_currency       = strtoupper($currency);
            // code reuse
            // Ref: https://stackoverflow.com/questions/12435556/format-a-float-to-two-decimal-places/12435576            
            $fmt_coin           = number_format($coin_converted,2);
            $fmt_coin_code      = strtoupper($myCoin->getCoin("code"));

            $msg                = $amount . " " . $coin_code . " in " . $fmt_currency . " is " . $fmt_coin;
            $result       = $convertLog->printLog("OK", $msg);
            $last_updated = "<b></br> [" . $myCoin->getCoin("last_updated") . "] 1 " . $fmt_coin_code . " = " . $myCoin->getCoin($currency) . " " . $fmt_currency . "</b>";
            $html_result  = array(
                $result, 
                $last_updated
            );
            return $html_result;
        } else {
            $msg         = "Failed to validate Amount: " . $amount;
            $result      = $convertLog->printLog("ERROR", $msg);
            $html_result = array($result);
            return $html_result;
        }

    } else {
        // handle incomplete input - (!empty($amount) && !empty($crypto) && !empty($currency)
        $msg         = "Please input Amount, Cryptocoin and Currency.";
        $result      = $convertLog->printLog("INFO", $msg);
        $html_result = array($result);
        return $html_result;        
    }

}
