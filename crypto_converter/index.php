<?php

/*
About
    Cryptocoin Converter

    Displays form to input Crypto coin amount, crypto coin,
    currency.

    Input is processed by includes/convert.php which returns coin amounts in selected currency
    and outputs Results.

    Drop-down menu currencies are defined in conf/config.php $available_currencies    Drop-down menu coins are defined in conf/config.php $available_coins
    Coin data is defined in conf/config.php $crypto_coins 2d array.

    New coins can be added by editing conf/config.php

        1. Adding a new currency into the $available_currencies array 
        2. Adding a new crypto coin into the $available_coins array
        3. Including an additional array in $crypto_coins using the following format;
        note point 1 must match keys: eur/zar (or another currency) and 2 must match the desc value, 
        for example:
        $crypto_coins = array(
        ...
        array(
            "desc"         => "bitcoin",
            "code"         => "BTC",
            "eur"          => 12272.77,
            "zar"          => 184707.34,
            "last_updated" => "2017-12-24",
        ),
    2. 

Requirements
    http://getbootstrap.com/docs/3.3/getting-started/#download
    conf/config.php $crypto_coins variable

Image Credits

    Logos
    https://blogs-images.forbes.com/laurashin/files/2017/07/Cryptocurrency-MobileCover.jpg
    http://bitemycoin.com//wp-content/uploads/2017/07/BiteMyCoin-Cryptocurrency.png

    favicon
    https://cryptocurrency.tadaawl.com/en/assets/images/favicon/favicon-192.png

Author
    Jay Maduray <jayendren@gmail.com>

*/

// includes

// code reuse
// Ref: https://stackoverflow.com/questions/29679022/form-and-php-result-display-on-same-page
//
// Require results to be displayed in the same window (not new window or iframe)
// therefore no form action is used in this code,
// results ($html_results) are retreived from the included convert.php file
include_once 'includes/convert.php';

?>

<!DOCTYPE html>
<html>
<head>
    <!-- code reuse
    Ref:
    https://www.bootstrapcdn.com
    http://tangledindesign.com/how-to-create-a-contact-form-using-html5-css3-and-php/
    https://bootsnipp.com/iframe/x95Q
    -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="CrytoCurrency Convertor">
    <meta name="author" content="Jay Maduray">
    <title>CryptoCurrency Convertor</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <style type="text/css">
    body {
        background-color: black;
        background: url("images/Cryptocurrency-MobileCover.jpg") no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
    .container{
    padding: 25px;
    }

    /* code reuse
    Ref: https://stackoverflow.com/questions/29356015/making-a-div-transparent-and-showing-background-image
    */
    div.container-fluid{
        /*-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";*/
        /*filter: alpha(opacity=0);   */
        /*-moz-opacity: 0.0;           */
        /*-khtml-opacity: 0.0;         */
        /*opacity: 0.0;                */
        /*background: rgba(255,255,255,0);*/
        /*border: 3px solid #373D3F;*/
        background-color: transparent !important;
        color: #373D3F;
        opacity: 0.6;
    }
    </style>
</head>

<body>
<!-- code reuse
Ref: https://www.viget.com/articles/color-contrast
-->
    <font color="#373D3F">

        <header class="body">
            <!-- code reuse
            Ref: https://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_jumbotron&stacked=h
            -->

             <div class="container-fluid" style="background:transparent;">
              <div class="jumbotron">
                <center>
                    <h1>CryptoCoin Converter</h1>
                    <p>Convert popular crypto coins into available currencies.</p>
                </center>
              </div>
            </div>
        </header>

        <section class="body">
            <div class="container">
                <div class="row">
                    <h1>Convert</h1>
                    <form class="form-inline" role="form" method='post'>

                        I want to convert

                        <input type=number step=0.01 id="amount" name='amount' placeholder='0,01'/>

                        <select class="form-control" name="crypto">
                          <option value="">Select...</option>

                          <?php foreach ($available_coins as $coins): ?>
                          <option value="<?php echo $coins; ?>"><?php echo strtoupper($coins); ?></option>
                          <?php endforeach;?>

                          </option>

                        </select>

                        to

                        <select class="form-control" name="currency">
                          <option value="">Select...</option>

                          <?php foreach ($available_currencies as $currencies): ?>
                          <option value="<?php echo $currencies; ?>"><?php echo strtoupper($currencies); ?>
                          <?php endforeach;?>

                          </option>

                        </select>

                        <input type="submit" value="Convert" class="btn btn-primary"/>

                    </form>

                    <?php                         
                        // return result array from convert.php
                        // index 0 = conversion results
                        // index 1 = last updated (manual sync)
                        $result = convert_coin(
                            $crypto_coins,
                            $form_amount,
                            $form_crypto,
                            $form_currency
                        );
                        $html_result  = $result[0];
                        $last_updated = $result[1];
                    ?>

                    <h1>Results</h1>
                    
                    <?php 
                        if ( !empty($html_result) ) {
                            echo $html_result;
                        } 
                    ?>

                    <h1>Last Updated</h1>

                    <?php 
                        if ( !empty($last_updated) ) {
                            echo $last_updated;
                        }
                    ?>

                </div>

            </div>

        </section>

        <footer class="footer">
            <!-- code reuse
            Ref: http://getbootstrap.com/docs/4.0/examples/sticky-footer/
            -->
            <div class="container">
                <span class="text-muted"><?php echo $footer ?></span>
            </div>
        </footer>


        <!-- code reuse
        Ref: https://www.bootstrapcdn.com
        -->
        <script src="js/bootstrap.min.js"></script>

    </font>

</body>

</html>
