<?php

/*
Validate.class.php
    Validates Coin input amounts

Methods
    isPositive([int|float])

Author
    Jay Maduray <jayendren@gmail.com>
*/

class Validate
{
    // properties
    private $amount;

    // constructs

    // code reuse
    // Ref: https://www.killerphp.com/tutorials/php-objects-page-3/
    public function __construct($amount)
    {
        // initialize the amount using validatePositive method
        $this->isPositive($amount);
    }

    // methods

    // validate coin amount is a postive number
    public function isPositive($amount)
    {
        // code reuse
        // Ref: http://php.net/manual/en/function.is-numeric.php
        // Ref: https://stackoverflow.com/a/4844936
        if (is_numeric($amount) && $amount > 0) {
            return true;
        } else {
            return false;

        }
    }

}
