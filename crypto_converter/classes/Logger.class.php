<?php

/*
Logger.class.php

    Generates html formated (bootstrap) messages, 
    Example: OK: 2018-01-03 12:58:47 0.25 bitcoin in ZAR is 46176.835
    
Methods
    printLog("[ERROR|WARNING|INFO|OK]", "message")


Author
    Jay Maduray <jayendren@gmail.com>
*/

// includes

class Logger
{
    // properties
    private $loglevel;
    private $msg;

    // constants
    //const HASHED_DATA = hashed_data;

    // constructs

    // code reuse
    // Ref: https://www.killerphp.com/tutorials/php-objects-page-3/
    public function __construct()
    {
        // initialize the coin properties using setCoin method
        $this->printLog(
            $loglevel,
            $msg
        );
    }

    // methods

    // return the coin properties based on input value
    public function printLog(
        $loglevel,
        $msg
    ) {
        if (!empty($loglevel) && !empty($msg)) {
            // get current date in YYYY-mm-dd format
            // code reuse
            // Ref: https://www.w3schools.com/php/php_date.asp
            $now = date("Y-m-d h:i:s");

            // code reuse
            // Ref: https://www.w3schools.com/php/php_switch.asp
            switch ($loglevel) {
                case "ERROR":
                    // code reuse
                    // Ref: https://www.w3schools.com/bootstrap/bootstrap_alerts.asp
                    $bootstrap = "<div class=\"alert alert-danger\"><strong>";
                    return $bootstrap . " ERROR: {$now} </strong>" . $msg . "</div>";
                    break;
                case "WARNING":
                    // code reuse
                    // Ref: https://www.w3schools.com/bootstrap/bootstrap_alerts.asp
                    $bootstrap = "<div class=\"alert alert-warning\"><strong>";
                    return $bootstrap . " WARNING: {$now} </strong>" . $msg . "</div>";
                    break;
                case "INFO":
                    // code reuse
                    // Ref: https://www.w3schools.com/bootstrap/bootstrap_alerts.asp
                    $bootstrap = "<div class=\"alert alert-info\"><strong>";
                    return $bootstrap . " INFO: {$now} </strong>" . $msg . "</div>";
                    break;
                case "OK":
                    // code reuse
                    // Ref: https://www.w3schools.com/bootstrap/bootstrap_alerts.asp
                    $bootstrap = "<div class=\"alert alert-success\"><strong>";
                    return $bootstrap . " OK: {$now} </strong>" . $msg . "</div>";
                    break;
                default:
                    return "unknown loglevel found: " . $loglevel . "</br>";
                    break;
            }
        } else {
            return false;
            echo "ERROR: loglevel & msg undefined - " . $loglevel . " " . $msg . "</br>";
        }
    }
}

//  $myLogger = new Logger;
//  echo $myLogger->printLog("ERROR", "this is a error msg");
