<?php

/*
Coin.class.php
    Object for cryptocoins

Methods
    setCoin(associative_array)
        sets the coin properties using an array as input

    getCoin("property")
        returns value of coin property

    validateCoin([int|float])
        uses Validate.class.php isPositive($amount) method


Author: Jay Maduray <jayendren@gmail.com>
*/

// includes

include "Validate.class.php"; // Validate amounts
include "Logger.class.php";  //  Return loglevel messages

class Coin
{
    // properties
    private $desc;
    private $code;
    private $eur;
    private $zar;
    private $last_updated;

    // constants
    //const HASHED_DATA = hashed_data;

    // constructs

    // code reuse
    // Ref: https://www.killerphp.com/tutorials/php-objects-page-3/
    public function __construct($hashed_data)
    {
        // initialize the coin properties using setCoin method
        $this->setCoin($hashed_data);
    }

    // methods

    // set the coin properties using an associative array as input
    public function setCoin($hashed_data)
    {
        // code reuse
        // Ref: http://php.net/manual/en/function.is-array.php
        // Ref: http://php.net/manual/en/function.is-object.php
        if (is_array($hashed_data) || is_object($hashed_data)) {

            // code reuse
            // Ref: https://stackoverflow.com/questions/1951690/how-to-loop-through-an-associative-array-and-get-the-key
            foreach ($hashed_data as $key => $value) {
                // uncomment to echo key->value pairs
                // echo "Key=" . $key . ", Value=" . $value;
                // echo "<br>";

                // code reuse
                // Ref: https://www.w3schools.com/php/php_switch.asp
                switch ($key) {
                    case "desc":
                        $this->desc = $value;
                        break;
                    case "code":
                        $this->code = $value;
                        break;
                    case "eur":
                        $this->eur = $value;
                        break;
                    case "zar":
                        $this->zar = $value;
                        break;
                    case "last_updated":
                        $this->last_updated = $value;
                        break;
                    default:
                        $msg           = "unknown key/value found: " . $key . "/" . $value;
                        $setCoinLogger = new Logger;
                        return $setCoinLogger->printLog("ERROR", $msg);
                        break;
                }
            }
        }
    }

    // return the coin properties based on input value
    public function getCoin($property)
    {
        if (!empty($this->$property)) {
            return $this->$property;
        } else {
            return false;
            $msg           = "invalid property " . $property;
            $setCoinLogger = new Logger;
            echo $setCoinLogger->printLog("ERROR", $msg);
        }
    }

    // validate coin amount is a postive number using isPositive method from Validate.class.php
    public function validateCoin($amount)
    {
        $validCoin = new Validate($amount);
        $result    = $validCoin->isPositive($amount);
        return $result;
    }

    // calculate value of amount in currency
    public function calcCoin(
        $amount,
        $value
    ) {
        $amount_value = ($amount * $value);
        return $amount_value;
    }

}
